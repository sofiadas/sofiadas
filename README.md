## Sofia's README

Hi, I'm Sofia Duarte - a Security Engineer for our Red Team.
This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

Please feel free to contribute to this page by opening a merge request.

## Related pages
Here are some resources to learn more about our team:

- [GitLab Handbook - Red Team](https://about.gitlab.com/handbook/security/threat-management/red-team/): An overview of our team.
- [Red Team Public](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public): Various resources we'd like to share with the community.

## About me
Some things about me:
- Started my career in a company development program rotating between SIRT, DevOps, Data Science, and Application Security to then going into pentesting and now red teaming.

## Communicating with me
- Got questions for Red Team? `#g_security_redteam`
- Please feel free to reach out about anything, I would be thrilled to hear from you!
- I would really appreciate any feedback that you think might help me do my job better. Drop me a message any time.

